## Open Annotations

Utility package to preview annotations in Papers Web Viewer.

### Installation

Please note that you must specify the below registry when installing the package:

`npm i --registry=https://npm.fury.io/readcube @readcube/open-in-papers`

### Example Usage

```
import { openAnnotations } from '@readcube/open-in-papers';

const pdfUrl = 'https://mozilla.github.io/pdf.js/web/compressed.tracemonkey-pldi-09.pdf';
const doi = '10.1145/1543135.1542528';

const annotations = [{
  "id": "ddea6e84-c44b-4f99-b40a-45e64a295f21",
  "type": "highlight",
  "rects": [
    [
       75.58,
       531.51,
       51.7,
       14.63
    ]
  ],
  "page_start": 3,
  "color_id": 2,
  "text": "Trace Trees"
}];

openAnnotations({
  pdfUrl,
  doi,
  annotations,
  annotationId: annotations[0].id
});
```

Alternatively, you can also include the package by linking to the latest version directly from our package server:
```
<script type="module">
  import { openAnnotations } from 'https://pkg.readcube.com/@readcube/open-in-papers@latest/index.js';
  // ...
</script>
```

Or using a specific version:
```
<script type="module">
  import { openAnnotations } from 'https://pkg.readcube.com/@readcube/open-in-papers@x.y.z/index.js'
  // ...
</script>
```

### Api

`openAnnotations({ pdfUrl, doi, tab, annotations, annotationId })`

* `pdfUrl: string` *(required)*
* `doi: string` *(optional)*
* `tab: Boolean` *(optional)*
* `annotations: Array` *(optional)*
* `annotationId: string` *(optional)*

Opens an article PDF supplied by `pdfUrl` in the Papers Web Viewer.

If you specify the `doi` parameter, automatic resolving of the article PDF metadata will be attempted.

The `tab` param when set to `true` can be used to open the Papers Web Viewer in a new browser tab.

The `annotations` param is an array of Papers Sync annotations which, when supplied, will be rendered on the PDF.

If the `annotationId` param is specified the annotation with this `id`, if found, will be scrolled into the viewport and selected.

### Local Testing

```
npm i
npm run start-annotations
```

Open `http://localhost:3500/test/annotations.html` in any *modern* browser.
