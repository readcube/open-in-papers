export default {
  background: {
    base: {
      'position': 'fixed',
      'top': '0', 'left': '0',
      'width': '100%',
      'height': '100%'
    },
    hide: {
      'visibility': 'hidden',
      'top': '-100%',
      'bottom': '100%',
      'z-index': '0'
    },
    show: {
      'visibility': 'visible',
      'background': '#e4e4e4',
      'top': '0%',
      'bottom': '0%',
      'z-index': '2100000000'
    }
  },
  frame: {
    base: {
      'position': 'absolute',
      'top': '0', 'left': '0',
      'width': '100%',
      'height': '100%',
      'border': 'none',
      'outline': 'none'
    },
    hide: {
      'visibility': 'hidden',
      'top': '-100%',
      'bottom': '100%'
    },
    show: {
      'visibility': 'visible',
      'top': '0%',
      'bottom': '0%'
    }
  }
};
