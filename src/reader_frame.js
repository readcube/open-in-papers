import $       from 'cash-dom';
import Eventer from '@readcube/eventer-js';
import styles  from './styles';

const MESSAGES  = [
  'loaded',
  'initialized',
  'redirect',
  'close'
];

class ReaderFrame {
  constructor() {
    this._src = null;
    this._ready = null;
    this._frame = null;
    this._background = null;
    this._onmessage = null;
    this._evtbus = Eventer({});
  }

  _createFrame() {
    this._frame && this._frame.remove();
    return this._frame = $('<iframe allowtransparency="true"></iframe>')
      .css(Object.assign(styles.frame.base, styles.frame.hide))
      .addClass('rc-reader-frame');
  }

  _createBackground() {
    this._background && this._background.remove();
    return this._background = $('<div></div>')
      .css(Object.assign(styles.background.base, styles.background.hide))
      .addClass('rc-reader-bg');
  }

  _unbindMessages() {
    window.removeEventListener('message', this._onmessage);
    this._onmessage = null;
  }

  _bindMessages() {
    this._unbindMessages();
    window.addEventListener('message', this._onmessage = e => {
      while (e.originalEvent) e = e.originalEvent;
      if (!/readcube\.com(:\d+)?$/.test(e.origin)) {
        return;
      }
      if (MESSAGES.indexOf(e.data.type) != -1) {
        this._evtbus.trigger(e.data.type, [e.data]);
      }
    });
  }

  _send(data) {
    if (!this._frame || !this._frame.length) return;
    this._frame[0].contentWindow.postMessage(data, this._src);
  }

  get evtout() {
    return this._evtbus;
  }

  open(src) {
    if (this._frame) {
      this.close();
    }

    this._src = src;
    this._createFrame();
    this._createBackground();
    this._bindMessages();

    this._evtbus.one('loaded', () => {
      this._send({ type: 'new_session' });
    });
    
    this._ready = new Promise(resolve => {
      this._evtbus.one('initialized', resolve);
    }).then(() => {
      this._background.css(styles.background.show);
      this._frame.css(styles.frame.show);
      this._frame[0].contentWindow.focus();
    });

    this._frame.attr('src', this._src);
    this._frame.appendTo(this._background);
    this._background.appendTo(document.body);

    return this._ready;
  }

  navigate(src) {
    this._src = src;
    this._frame.attr('src', this._src);
  }

  send(data) {
    if (this._ready) {
      this._ready.then(() => this._send(data));
    }
  }

  close() {
    this._evtbus.off();
    this._unbindMessages();

    this._src = null;
    this._ready = null;

    this._frame.remove();
    this._frame = null;

    this._background.remove();
    this._background = null;
  }
}

export default ReaderFrame;
