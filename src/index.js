import ReaderFrame  from './reader_frame';
import ReaderWindow from './reader_window';
import { viewerSrc, downloadBuffer } from './helper';

const readerFrame   = new ReaderFrame();
const readerWindow  = new ReaderWindow();

function openFrame({
  src,
  extIds,
  pdfUrl,
  pdfBuffer,
  pdfDownload,
  addToLibrary,
  customMetadata,
  importData,
  articleData,
  annotations,
  annotationId
}) {
  const progress = state => {
    const type = 'download-progress';
    readerFrame.send({ type, state });
  };
  const download = pdfDownload ?
    pdfDownload(progress) :
    downloadBuffer({ pdfUrl, pdfBuffer, progress });

  download.then(pdf => {
    const type = 'pdf';
    readerFrame.send({
      type,
      pdf,
      extIds,
      addToLibrary,
      customMetadata,
      importData,
      articleData,
      annotations,
      annotationId
    });
  }).catch(reason => {
    if (reason.redirect) {
      readerFrame.navigate(reason.redirect);
    }
  });

  readerFrame.evtout.on('redirect', (e, data) => location = data.url);
  readerFrame.evtout.on('close', () => readerFrame.close());

  return readerFrame.open(src);
}

function openWindow({
  src,
  extIds,
  pdfUrl,
  pdfBuffer,
  pdfDownload,
  addToLibrary,
  customMetadata,
  importData,
  articleData,
  annotations,
  annotationId
}) {
  const progress = state => {
    const type = 'download-progress';
    readerWindow.send({ type, state });
  };
  const download = pdfDownload ?
    pdfDownload(progress) :
    downloadBuffer({ pdfUrl, pdfBuffer, progress });

  download.then(pdf => {
    const type = 'pdf';
    readerWindow.send({
      type,
      pdf,
      extIds,
      addToLibrary,
      customMetadata,
      importData,
      articleData,
      annotations,
      annotationId
    });
  }).catch(reason => {
    if (reason.redirect) {
      readerWindow.navigate(reason.redirect);
    }
  });

  return readerWindow.open(src);
}

function openInPapers({
  pdfUrl,
  pdfBuffer,
  pdfDownload,
  doi,
  pmid,
  originalId,
  addToLibrary,
  customMetadata,
  articleData,
  tab,
  env
}) {
  env = env != 'staging' ? 'production' : 'staging';

  const extIds = { doi, pmid };
  const src = viewerSrc({ env, extIds, tab });
  const importData = { original_id: originalId, imported_by: 'open-in-papers' };
  const data = {
    src,
    extIds,
    pdfUrl,
    pdfBuffer,
    pdfDownload,
    addToLibrary,
    customMetadata,
    importData,
    articleData
  };

  return tab ? openWindow(data) : openFrame(data);
}

function openAnnotations({
  pdfUrl,
  pdfBuffer,
  pdfDownload,
  doi,
  annotations,
  annotationId,
  tab,
  env
}) {
  env = env != 'staging' ? 'production' : 'staging';

  const extIds = { doi };
  const src = viewerSrc({ env, extIds, tab }) + '&direct_annotations=1';
  const data = {
    src, extIds, pdfUrl, pdfBuffer, pdfDownload, annotations, annotationId
  };

  return tab ? openWindow(data) : openFrame(data);
}

export default openInPapers;
export { openAnnotations };
