import Eventer from '@readcube/eventer-js';

const MESSAGES  = [
  'initialized'
];

class ReaderTab {
  constructor() {
    this._src = null;
    this._window = null;
    this._onmessage = null;
    this._evtbus = Eventer({});
  }

  _send(data) {
    if (!this._window) return;
    this._window.postMessage(data, this._src);
  }

  _unbindMessages() {
    window.removeEventListener('message', this._onmessage);
    this._onmessage = null;
  }

  _bindMessages() {
    this._unbindMessages();
    window.addEventListener('message', this._onmessage = e => {
      while (e.originalEvent) e = e.originalEvent;
      if (!/readcube\.com(:\d+)?$/.test(e.origin)) {
        return;
      }
      if (MESSAGES.indexOf(e.data.type) != -1) {
        this._evtbus.trigger(e.data.type, [e.data]);
      }
    });
  }

  open(src) {
    this._bindMessages();
    this._src = src;
    this._ready = new Promise(resolve => {
      this._evtbus.one('initialized', resolve);
    });
    this._window = window.open(this._src, '_blank');
    return this._ready;
  }

  navigate(src) {
    this._src = src;
    this._window.location = src;
  }

  send(data) {
    if (this._ready) {
      this._ready.then(() => this._send(data));
    }
  }

  close() {
    this._evtbus.off();
    this._unbindMessages();

    this._window.close();
    this._window = null;

    this._ready = null;
    this._src = null;
  }
}

export default ReaderTab;
