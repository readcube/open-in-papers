export default {
  production: {
    web: 'https://www.readcube.com'
  },
  staging: {
    web: 'https://staging.readcube.com'
  }
};
