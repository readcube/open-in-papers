import endpoint from './endpoint';

function randomDoi() {
  return '10.0000/' + (Math.random() + 1).toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
}

function downloadBuffer({ pdfUrl, pdfBuffer, progress }) {
  if (pdfBuffer) {
    return Promise.resolve(pdfBuffer);
  }
  if (downloadBuffer[pdfUrl]) {
    return downloadBuffer[pdfUrl];
  }
  return downloadBuffer[pdfUrl] = new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', pdfUrl, true);
    xhr.responseType = 'arraybuffer';
    xhr.onprogress = ({ loaded, total }) => {
      isNaN(total) ?
        progress && progress({ loaded }) :
        progress && progress({ loaded, total });
    };
    xhr.onloadend = () => {
      xhr.status == 200 ?
        resolve(xhr.response) :
        reject({
          error: xhr.statusText,
          status: xhr.status
        });
    };
    xhr.send();
  });
}

function viewerSrc({ env, extIds, tab = false }) {
  let doi = extIds.doi || randomDoi();
  let src = `${endpoint[env].web}/articles/${doi}?direct_pdf=1`;
  if (!tab) src += `&parent_url=${encodeURIComponent(location)}`;
  return src;
}

export { downloadBuffer, viewerSrc };
