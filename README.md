## Open In Papers

Utility package to open PDF in Papers Web Viewer.

### Installation

Please note that you must specify the below registry when installing the package:

`npm i --registry=https://npm.fury.io/readcube @readcube/open-in-papers`

### Example Usage

```
import openInPapers from '@readcube/open-in-papers';

const pdfUrl = 'https://mozilla.github.io/pdf.js/web/compressed.tracemonkey-pldi-09.pdf';
const doi = '10.1145/1543135.1542528';

openInPapers({
  pdfUrl, doi,
  originalId: '12345',
  addToLibrary: true,
  customMetadata: { url: pdfUrl },
  articleData: { title: 'TraceMonkey' }
});
```

Alternatively, you can also include the package by linking to the latest version directly from our package server:
```
<script type="module">
  import openInPapers from 'https://pkg.readcube.com/@readcube/open-in-papers@latest/index.js';
  // ...
</script>
```

Or using a specific version:
```
<script type="module">
  import openInPapers from 'https://pkg.readcube.com/@readcube/open-in-papers@x.y.z/index.js'
  // ...
</script>
```

### Api

`openInPapers({ pdfUrl, pdfBuffer, pdfDownload, doi, pmid, originalId, addToLibrary, customMetadata, articleData, tab })`

* `pdfUrl: string` *(required if no pdfBuffer)*
* `pdfBuffer: ArrayBuffer` *(required if no pdfUrl)*
* `pdfDownload: function(progress)` *(optional)*
* `originalId: string` *(optional)*
* `doi: string` *(optional)*
* `pmid: string` *(optional)*
* `addToLibrary: Boolean` *(optional)*
* `customMetadata: Object (JSON)` *(optional)*
* `articleData: Object (JSON)` *(optional)*
* `tab: Boolean` *(optional)*

Opens an article PDF supplied by `pdfUrl` or `pdfBuffer` in the Papers Web Viewer.

Alternatively, if `pdfBuffer` and `pdfUrl` are not supplied, you can supply a `pdfDownload` function which accepts a `progress` callback and returns a promise that resolves with the pdf buffer as the result. If it rejects the promise, you can pass a `redirect` url as a property of the rejection reason object.

The article will automatically be imported to your *Papers Library* if you set `addToLibrary: true`.

If you specify any of the `doi` or `pmid` parameters, automatic resolving of the article PDF metadata will be attempted.

If you specify `originalId, doi or pmid` and a reference with the *same* `originalId, doi or pmid` already exists in your library, the PDF with metadata will be added to that reference, otherwise a new reference will be created. If you do not specify *any* of these a fallback to the PDF sha256 as the `originalId` will be used.

The `originalId` is stored in the reference under `import_data.original_id`.

The `customMetadata` JSON param can be used to pass any *custom* metadata which will be attached to the article when it is added to your Papers Library.

The `articleData` JSON param can be used to pass any article metadata which will be attached to the article when it is added to your Papers Library. This will *only* occur in the scenario when the article PDF is *not* automatically resolved by `doi` or `pmid`. The allowed properites are: `abstract, authors, eissn, issn, issue, journal, journal_abbrev, pagination, title, volume, year, isbn, eisbn, chapter`.

The `tab` param when set to `true` can be used to open the Papers Web Viewer in a new browser tab.

### Local Testing

```
npm i
npm start
```

Open `http://localhost:3500/test/` in any *modern* browser.
